package com.epam.training.diego_cordero.optional_taks3.pages;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage {
    WebDriver driver;
    private WebDriverWait wait;
    By searchBox = By.xpath("//*[@id=\"i4\"]");

    public HomePage(WebDriver driver) {
        this.driver = driver;
    }

    public void clickSearchIcon() {
        WebElement searchIcon =driver.findElement(By.xpath("/html/body/c-wiz[2]/header/div[2]/div[1]/div/div[2]/div[2]/div[1]/div/div"));
        searchIcon.click();
    }

    public void enterSearchQuery(String query) {
        driver.findElement(searchBox).sendKeys(query);
        driver.findElement(searchBox).submit();
    }
}