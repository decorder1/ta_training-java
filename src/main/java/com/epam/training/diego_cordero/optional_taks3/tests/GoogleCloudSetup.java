package com.epam.training.diego_cordero.optional_taks3.tests;

import com.epam.training.diego_cordero.optional_taks3.pages.HomePage;
import com.epam.training.diego_cordero.optional_taks3.pages.PricingCalculatorPage;
import com.epam.training.diego_cordero.optional_taks3.pages.SearchResultsPage;
import com.epam.training.diego_cordero.optional_taks3.utils.PropertyLoader;
import com.epam.training.diego_cordero.optional_taks3.utils.TestListener;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import static org.testng.Assert.assertTrue;



@Listeners(TestListener.class)
public class GoogleCloudSetup {
    WebDriver driver;
    HomePage homePage;
    SearchResultsPage searchResultsPage;
    PricingCalculatorPage pricingCalculatorPage;
    PropertyLoader.WebDriverManagerUtil webDriverManagerUtil;
    PropertyLoader propertyLoader;

    @BeforeClass
    public void setUp() {
        System.setProperty("webdriver.gecko.driver","src/main/geckodriver");
        System.setProperty("webdriver.firefox.bin", "/Applications/Firefox.app/Contents/MacOS/firefox");
        //webDriverManagerUtil = new WebDriverManagerUtil();
        //driver = webDriverManagerUtil.getDriver("firefox");
        //driver.get("https://cloud.google.com/");

        String environment = System.getProperty("env", "dev"); // Default to 'dev' if no env is specified
        propertyLoader = new PropertyLoader("dev");
        webDriverManagerUtil = new PropertyLoader.WebDriverManagerUtil(propertyLoader);
        driver = webDriverManagerUtil.getDriver();
        driver.get(propertyLoader.getProperty("baseUrl"));
        homePage = new HomePage(driver);
        searchResultsPage = new SearchResultsPage(driver);
        pricingCalculatorPage = new PricingCalculatorPage(driver);
    }
    public WebDriver getDriver() {
        return driver;
    }
    @Test(groups = { "smoke" })
    public void testGoogleCloudPricingCalculatorSmoke() {
        String searchString="Google Cloud Platform Pricing Calculator";
        String cost="556";
        homePage.clickSearchIcon();
        homePage.enterSearchQuery(searchString);
        searchResultsPage.clickFirstResult();
        pricingCalculatorPage.addToEstimate();
        pricingCalculatorPage.fillOutEstimateForm();
        String totalCost = pricingCalculatorPage.getTotalEstimatedCost();
        assertTrue(totalCost.contains(cost));
    }
    @Test(groups = { "regression" })
    public void testGoogleCloudPricingCalculatorRegression() {
        String searchString="Google Cloud Platform Pricing Calculator";
        String cost="556";
        homePage.clickSearchIcon();
        homePage.enterSearchQuery(searchString);
        searchResultsPage.clickFirstResult();
        pricingCalculatorPage.addToEstimate();
        pricingCalculatorPage.fillOutEstimateForm();
        String totalCost = pricingCalculatorPage.getTotalEstimatedCost();
        assertTrue(totalCost.contains(cost));
    }
    @AfterClass
    public void tearDown() {
        driver.quit();
    }
}
