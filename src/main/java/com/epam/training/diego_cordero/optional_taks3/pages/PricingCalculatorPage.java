package com.epam.training.diego_cordero.optional_taks3.pages;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.time.Instant;

public class PricingCalculatorPage {
    WebDriver driver;
    private WebDriverWait wait;

    By addToEstimateButton = By.cssSelector(".UywwFc-vQzf8d");
    By numberOfInstances = By.id("c11");
    By operatingSystem = By.xpath("/html/body/c-wiz[1]/div/div/div[1]/div/div/div/div/div/div/div/div[1]/div/div[2]/div[3]/div[7]/div/div[1]/div/div/div/div[1]/span[1]");
   By provisioningModel = By.xpath("/html/body/c-wiz[1]/div/div/div[1]/div/div/div/div/div/div/div/div[1]/div/div[2]/div[3]/div[9]/div/div/div[2]/div/div/div[1]/label");
    By machineFamily = By.id("c25");
    By series = By.id(".VfPpkd-xl07Ob-XxIAqe-OWXEXe-FNFY6c");
    By machineType = By.xpath("/html/body/c-wiz[1]/div/div/div[1]/div/div/div/div/div/div/div/div[1]/div/div[2]/div[3]/div[11]/div/div/div[2]/div/div[1]/div[3]/div/div/div/div[1]/div");
    By addGPUsCheckbox = By.xpath("//md-checkbox[@aria-label='Add GPUs']");
    By gpuType = By.id("select_406");
    By numberOfGPUs = By.id("input_408");
    By localSSD = By.id("select_413");
    By datacenterLocation = By.id("select_418");
    By committedUsage = By.id("select_421");
    By totalEstimatedCost = By.xpath("/html/body/c-wiz[1]/div/div/div[1]/div/div/div/div/div/div/div/div[2]/div[1]/div/div[4]/div[1]/div[2]/label");

    public PricingCalculatorPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 10);
    }

    public void addToEstimate() {
        driver.findElement(addToEstimateButton).click();
        WebElement computeEngine=driver.findElement(By.cssSelector("div.VobRQb:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > p:nth-child(2)"));
        computeEngine.click();
    }

    public void fillOutEstimateForm() {
        driver.findElement(numberOfInstances).clear();
        driver.findElement(numberOfInstances).sendKeys("4");


       // WebElement provisioningModel = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(("//button[text()='Regular']"))));
        //provisioningModel.click();

        //driver.findElement(provisioningModel).click();
       // new Select(driver.findElement(machineFamily)).selectByVisibleText("General purpose");
       // new Select(driver.findElement(series)).selectByVisibleText("N1");
      //  new Select(driver.findElement(machineType)).selectByVisibleText("n1-standard-8 (vCPUs: 8, RAM: 30 GB)");
        //driver.findElement(addGPUsCheckbox).click();
        //new Select(driver.findElement(gpuType)).selectByVisibleText("NVIDIA Tesla V100");
       // driver.findElement(numberOfGPUs).sendKeys("1");
       // new Select(driver.findElement(localSSD)).selectByVisibleText("2x375 GB");
       // new Select(driver.findElement(datacenterLocation)).selectByVisibleText("Frankfurt (europe-west3)");
       // new Select(driver.findElement(committedUsage)).selectByVisibleText("1 Year");
    }

    public String getTotalEstimatedCost() {
        return driver.findElement(totalEstimatedCost).getText();
    }
}