package com.epam.training.diego_cordero.optional_taks3.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Instant;

public class SearchResultsPage {
    WebDriver driver;
   WebDriverWait wait;



    public SearchResultsPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 10);
    }

    public void clickFirstResult() {

        WebElement firstResult= wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("div.wVBoU:nth-child(1) > div:nth-child(1) > div:nth-child(1) > a:nth-child(1)")));
        firstResult.click();
    }
}