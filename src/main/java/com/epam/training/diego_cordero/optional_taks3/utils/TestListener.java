package com.epam.training.diego_cordero.optional_taks3.utils;

import com.epam.training.diego_cordero.optional_taks3.tests.GoogleCloudSetup;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.openqa.selenium.WebDriver;

public class TestListener implements ITestListener {

    @Override
    public void onTestFailure(ITestResult result) {
        Object testClass = result.getInstance();
        WebDriver driver = ((GoogleCloudSetup) testClass).getDriver();
        ScreenshotUtil.takeScreenshot(driver, result.getName());
    }

    @Override
    public void onTestStart(ITestResult result) {
        // Not implemented
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        // Not implemented
    }

    @Override
    public void onTestSkipped(ITestResult result) {
        // Not implemented
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
        // Not implemented
    }

    @Override
    public void onStart(ITestContext context) {
        // Not implemented
    }

    @Override
    public void onFinish(ITestContext context) {
        // Not implemented
    }
}
