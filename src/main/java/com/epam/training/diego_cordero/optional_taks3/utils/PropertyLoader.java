package com.epam.training.diego_cordero.optional_taks3.utils;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyLoader {
    private Properties properties = new Properties();

    public PropertyLoader(String environment) {
        String propertiesFileName = environment + ".properties";
        try (InputStream input = getClass().getClassLoader().getResourceAsStream(propertiesFileName)) {
            if (input == null) {
                throw new RuntimeException("Sorry, unable to find " + propertiesFileName);
            }
            properties.load(input);
        } catch (IOException ex) {
            ex.printStackTrace();
            throw new RuntimeException("Failed to load properties file: " + propertiesFileName);
        }
    }

    public String getProperty(String key) {
        return properties.getProperty(key);
    }

    public static class WebDriverManagerUtil {
        private WebDriver driver;
        private PropertyLoader propertyLoader;

        public WebDriverManagerUtil(PropertyLoader propertyLoader) {
            this.propertyLoader = propertyLoader;
        }

        public WebDriver getDriver() {
            String browser = propertyLoader.getProperty("browser");
            switch (browser.toLowerCase()) {
                case "firefox":
                    WebDriverManager.firefoxdriver().setup();
                    driver = new FirefoxDriver();
                    break;
                case "chrome":
                    WebDriverManager.chromedriver().setup();
                    driver = new ChromeDriver();
                    break;
                case "edge":
                    WebDriverManager.edgedriver().setup();
                    driver = new EdgeDriver();
                    break;
                default:
                    throw new IllegalArgumentException("Browser not supported: " + browser);
            }
            driver.manage().window().maximize();
            return driver;
        }

        public void quitDriver() {
            if (driver != null) {
                driver.quit();
            }
        }
    }
}
