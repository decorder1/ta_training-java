package com.epam.training.diego_cordero.optional_task1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PasteBinPage {
    private WebDriver driver;
    private WebDriverWait wait;

    public PasteBinPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 10);
    }

    // Navigate to Pastebin
    public void openPastebin() {
        driver.get("https://pastebin.com/");
    }

    // Enter code into the code input field
    public void enterCode(String code) {
        WebElement codeInput = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("postform-text")));
        codeInput.sendKeys(code);
    }

    // Select "10 Minutes" from Paste Expiration dropdown
    public void selectPasteExpiration() {
        WebElement expirationDropdown = wait.until(ExpectedConditions.elementToBeClickable(By.id("select2-postform-expiration-container")));
        expirationDropdown.click();

        WebElement option = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//li[text()='10 Minutes']")));
        option.click();
    }

    // Enter paste name
    public void enterPasteName(String name) {
        WebElement pasteNameInput = driver.findElement(By.id("postform-name"));
        pasteNameInput.sendKeys(name);
    }

    // Create new paste
    public void createNewPaste() {
        WebElement createPasteButton = driver.findElement(By.xpath("//button[text()='Create New Paste']"));
        createPasteButton.click();
    }

    // Get the URL of the created paste
    public String getPasteURL() {
        return driver.getCurrentUrl();
    }
}