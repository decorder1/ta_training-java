package com.epam.training.diego_cordero.optional_task1;

import com.epam.training.diego_cordero.optional_task1.PasteBinPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class PasteBinSetup {
    private WebDriver driver;
    private PasteBinPage pasteBinPage;

    @BeforeTest
    public void setUp() {
        // Set the path to GeckoDriver executable
        System.setProperty("webdriver.gecko.driver","src/main/geckodriver");
        System.setProperty("webdriver.firefox.bin", "/Applications/Firefox.app/Contents/MacOS/firefox");

        // Initialize FirefoxDriver
        driver = new FirefoxDriver();

        // Initialize the PasteBinPage object
        pasteBinPage = new PasteBinPage(driver);
    }

    @Test
    public void createNewPaste() {
        // Open Pastebin
        pasteBinPage.openPastebin();

        // Enter code
        pasteBinPage.enterCode("Hello from WebDriver");

        // Select Paste Expiration
        pasteBinPage.selectPasteExpiration();

        // Enter paste name
        pasteBinPage.enterPasteName("helloweb");

        // Create new paste
        pasteBinPage.createNewPaste();

        // Get and print the URL of the created paste
        String pasteURL = pasteBinPage.getPasteURL();
        System.out.println("Paste URL: " + pasteURL);
    }

    @AfterTest
    public void tearDown() {
        // Close the browser
        if (driver != null) {
            driver.quit();
        }
    }
}
