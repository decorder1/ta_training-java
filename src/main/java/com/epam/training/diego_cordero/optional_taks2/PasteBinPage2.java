package com.epam.training.diego_cordero.optional_taks2;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PasteBinPage2 {
    private WebDriver driver;
    private WebDriverWait wait;

    public PasteBinPage2(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 10);
    }

    // Navigate to Pastebin
    public void openPastebin() {
        driver.get("https://pastebin.com/");
    }

    // Enter code into the code input field
    public void enterCode(String code) {
        WebElement codeInput = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("postform-text")));
        codeInput.sendKeys(code);
    }

    // Select Syntax Highlighting option
    public void selectSyntaxHighlighting(String syntax) {
        WebElement syntaxDropdown = driver.findElement(By.id("select2-postform-format-container"));
        syntaxDropdown.click();
        WebElement syntaxOption = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//li[text()='" + syntax + "']")));
        syntaxOption.click();
    }

    // Select Paste Expiration option
    public void selectPasteExpiration(String expiration) {
        WebElement expirationDropdown = driver.findElement(By.id("select2-postform-expiration-container"));
        expirationDropdown.click();
        WebElement expirationOption = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//li[text()='" + expiration + "']")));
        expirationOption.click();
    }

    // Enter paste name
    public void enterPasteName(String name) {
        WebElement pasteNameInput = driver.findElement(By.id("postform-name"));
        pasteNameInput.sendKeys(name);
    }

    // Create new paste
    public void createNewPaste() {
        WebElement createPasteButton = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[text()='Create New Paste']")));
        //WebElement createPasteButton = driver.findElement(By.xpath("//button[text()='Create New Paste']"));
        createPasteButton.click();
    }

    // Get the URL of the created paste
    public String getPasteURL() {
        return driver.getCurrentUrl();
    }

    // Get the page title
    public String getPageTitle() {
        return driver.getTitle();
    }

    // Get the syntax highlight language
    public String getSyntaxHighlighting() {
        return driver.findElement(By.xpath("//a[@class='btn -small h_800']")).getText();
    }

    // Get the code content
    public String getCodeContent() {
        return driver.findElement(By.xpath("//textarea[@class='textarea']")).getText();
    }
}