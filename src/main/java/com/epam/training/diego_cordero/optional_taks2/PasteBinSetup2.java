package com.epam.training.diego_cordero.optional_taks2;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class PasteBinSetup2 {
    private WebDriver driver;
    private PasteBinPage2 pasteBinPage;

    @BeforeTest
    public void setUp() {
        // Set the path to GeckoDriver executable
        System.setProperty("webdriver.gecko.driver","src/main/geckodriver");
        System.setProperty("webdriver.firefox.bin", "/Applications/Firefox.app/Contents/MacOS/firefox");

        // Initialize FirefoxDriver
        driver = new FirefoxDriver();

        // Initialize the PasteBinPage2 object
        pasteBinPage = new PasteBinPage2(driver);
    }

    @Test
    public void createNewPaste() {
        // Open Pastebin
        pasteBinPage.openPastebin();

        // Enter code
        String code = "git config --global user.name  \"New Sheriff in Town\"\n" +
                "git reset $(git commit-tree HEAD^{tree} -m \"Legacy code\")\n" +
                "git push origin master --force";
        pasteBinPage.enterCode(code);

        // Select Syntax Highlighting
        pasteBinPage.selectSyntaxHighlighting("Bash");

        // Select Paste Expiration
        pasteBinPage.selectPasteExpiration("10 Minutes");

        // Enter paste name
        String pasteName = "how to gain dominance among developers";
        pasteBinPage.enterPasteName(pasteName);

        // Create new paste
        pasteBinPage.createNewPaste();

        // Validate the page title
        String pasteNameActual="how to gain dominance among developers - Pastebin.com";
        Assert.assertEquals(pasteBinPage.getPageTitle(), pasteNameActual, "Page title does not match the paste name");

        // Validate the syntax highlighting
        Assert.assertEquals(pasteBinPage.getSyntaxHighlighting(), "Bash", "Syntax highlighting does not match 'Bash'");

        // Validate the code content
        Assert.assertEquals(pasteBinPage.getCodeContent(), code, "Code content does not match the expected code");
    }

    @AfterTest
    public void tearDown() {
        // Close the browser
        if (driver != null) {
            driver.quit();
        }
    }
}