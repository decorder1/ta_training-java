package com.epam.training.diego_cordero;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;


public class PasteBinAutomation {

    public static void main(String[] args) {


        // Initialize ChromeDriver

        //System.setProperty("webdriver.chrome.driver","/Users/decorder/Desktop/chromedriver_mac64/chromedriver");
        System.setProperty("webdriver.gecko.driver","/Users/decorder/Desktop/driver/geckodriver");
        System.setProperty("webdriver.firefox.bin", "/Applications/Firefox.app/Contents/MacOS/firefox");
        //System.setProperty("webdriver.chrome.driver","Applications/chromedriver");
       //WebDriver driver = new ChromeDriver();
        WebDriver driver = new FirefoxDriver();

        // Open the desired URL
        driver.get("https://pastebin.com/");


        // Wait for the New Paste modal to appear
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("postform-text")));

        // Fill in the form fields
        WebElement codeInput = driver.findElement(By.id("postform-text"));
        codeInput.sendKeys("Hello from WebDriver");


        // Find the Paste Expiration dropdown element

        WebElement element=driver.findElement(By.id("select2-postform-expiration-container"));
        Actions actions= new Actions(driver);

        element.click();

        wait.until(ExpectedConditions.presenceOfElementLocated(By.className("select2-results__option")));
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//li[text()='10 Minutes']")));
        WebElement option=driver.findElement(By.xpath("//li[text()='10 Minutes']"));
        option.click();

        // Add title helloweb
        WebElement pasteNameInput = driver.findElement(By.id("postform-name"));
        pasteNameInput.sendKeys("helloweb");

        // Submit the form
        WebElement createPasteButton = driver.findElement(By.xpath("//button[text()='Create New Paste']"));
        createPasteButton.click();


        // Get the URL of the created paste
        String pasteURL = driver.getCurrentUrl();
        System.out.println("Paste URL: " + pasteURL);

        // Close the browser
        driver.quit();
    }
}

